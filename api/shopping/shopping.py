from datetime import date
from logging import debug
from flask import Flask, request, jsonify, Blueprint
import mysql.connector
from functools import wraps
from mysqlhelper import *
import hashlib



app = Flask(__name__)
my = MysqlHelper()    




@app.route('/shopping/', methods=['POST'])
def createshopping():
    try:
        data = request.json
        if "createddate" in data and "name" in data:
            data = {
                "Name": str(data['name']),
                "CreatedDate": str(data['createddate'])
            }
            result = my.insert("shopping", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"CreatedDate, id, Name", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/shopping/", methods=['GET'])
def getallshopping():
    try:
        result = my.get("shopping")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/shopping/<int:id>", methods=['GET'])
def betshoppingbyid(id):
    try:
        result = my.get_where("shopping", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/shopping/<int:id>", methods=['PUT'])
def updateshoppingbyid(id):
    try:
        data = request.json
        if "createddate" in data and "name" in data:
            data = {
                "Name": str(data['name']),
                "CreatedDate": str(data['createddate'])
            }
            result = my.update("shopping", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"nama, jumlah, status", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/shopping/<int:id>", methods=['DELETE'])
def deleteshoppingbyid(id):
    try:
        result = my.delete("shopping", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    app.run(host="0.0.0.0", port=5000)
    # app.run(debug=True, host="0.0.0.0", port=5001)
    # app.run(debug=True, port=5000)